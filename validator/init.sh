#!/bin/bash

SCRIPT_PATH=$(dirname $(realpath -s $0))
cd ${SCRIPT_PATH}
set -o allexport
source .env
source ../utilities/rainbow.sh
set +o allexport

~/utilities/create-data-volume.sh

curl https://sh.rustup.rs -sSf | sh -s -- -y
source $HOME/.cargo/env
rustup update

git clone https://github.com/paritytech/polkadot.git &&
	cd polkadot &&
	git tag | grep "$v\0\.8" &&
	git checkout v0.8.8 &&
	./scripts/init.sh &&
	cargo build --release

sed -i "s/<POKT_ADDRESS>/${POCKET_ADDRESS}/g" .env
sed -i "s#<POKT_RPC_URL>#https://pokt${VALIDATOR_NUMBER}-${REGION}.alphavirtual.com:443#g" .env
sed -i "s/<moniker>/av-${REGION}-${VALIDATOR_NUMBER}-${POCKET_ADDRESS}/g" /data/pokt/config/config.json

sed -i "s#<pokt_url>#https://pokt${VALIDATOR_NUMBER}-${REGION}.alphavirtual.com#g" /data/pokt/config/chains.json
sed -i "s#<eth_mainnet_url>#${ETH_MAINNET_URL}#g" /data/pokt/config/chains.json
source .env
./bootstrap.sh
sudo cp ./pokt.service /etc/systemd/system/
sudo systemctl enable pokt
sudo systemctl start pokt
echocyan "SLEEPING..."
sleep 60
./catchup.sh
../utilities/create-snapshot.sh poktchaindata:${CHAIN_ID}
#./stake.sh
