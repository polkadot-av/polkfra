#!/bin/bash

SCRIPT_PATH=$(dirname $(realpath -s $0))
source ${SCRIPT_PATH}/.env
source ${SCRIPT_PATH}/../utilities/rainbow.sh

BPS=0
LAST_RUN_BLOCK=0

function catchup() {

	local OURS=$(curl -sSX POST http://localhost:8081/v1/query/height | jq -r '.height')
	local THEIRS=$(curl -sSX POST ${HUB} | jq -r '.height')

	if [[ ${LAST_RUN_BLOCK} -gt 0 && ${OURS-=0} -gt 0 ]]; then
		BPS=$(((${OURS} - ${LAST_RUN_BLOCK}) / 5))
	fi

	tput cuu 1 && tput el
	echocyan "⇅ ${OURS:=0}/${THEIRS:=0} ${BPS} bps"

	if [[ ${THEIRS:=0} -gt ${OURS:=0} ]]; then
		LAST_RUN_BLOCK=${OURS}
		sleep 5
		catchup
	fi
}

catchup
echogreen "✓ Synced!"