#!/bin/bash

local IP=$(terraform output ${1}-ip)
local USER=$(terraform output prefix)
ssh ${USER}@${IP} "cd ~/${1}; exec \${SHELL} -c 'docker-compose down'"
scp -r ../${1}/ ${USER}@${IP}:/home/${USER}/
ssh ${USER}@${IP} "cd ~/${1}; exec \${SHELL} -c \"HOSTNAME=\${HOSTNAME} IP=${IP} docker-compose up -d\""