data "aws_route53_zone" "root" {
  name         = var.root_domain
  private_zone = false
}

resource "aws_route53_record" "validator" {
  zone_id = data.aws_route53_zone.root.zone_id
  count   = var.validator_count[terraform.workspace]
  name    = "${var.prefix}${count.index + 1}-${var.workspace_regions[terraform.workspace]}.${var.root_domain}"
  type    = "A"
  alias {
    name                   = aws_elb.rpc[count.index].dns_name
    zone_id                = aws_elb.rpc[count.index].zone_id
    evaluate_target_health = true
  }
}
