output "validator-ip" {
  value = aws_instance.validator.*.public_ip
}
output "prefix" {
  value = var.prefix
}
output "env" {
  value = terraform.workspace
}
