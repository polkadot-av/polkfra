variable "workspace_regions" {
  default = {
    default = "us-west-2"
    dev     = "us-west-1"
    stage   = "eu-west-1"
    prod    = "us-east-1"
  }
}

variable "validator_instance_types" {
  default = {
    default = "t3a.medium"
    dev     = "m5a.large"
    stage   = "m5a.large"
    prod    = "m5a.large"
  }
}

variable "validator_count" {
  default = {
    default = 1
    dev     = 1
    stage   = 1
    prod    = 2
  }
}

variable "prefix" {
  description = "Name of project being deployed for naming and tagging"
}

variable "ssh_port" {
  description = "sshd daemon listening port"
  default     = "22"
}

variable "public_key_path" {
  description = <<DESCRIPTION
Path to the SSH public key to be used for authentication.

Example: ~/.ssh/terraform.pub
DESCRIPTION

  default = "~/.ssh/id_rsa.pub"
}

variable "private_key_path" {
  description = <<DESCRIPTION
Path to the SSH private key to be used for authentication.

Example: ~/.ssh/id_rsa
DESCRIPTION

  default = "~/.ssh/id_rsa"
}

variable "validator_root_volume_size" {
  description = "Desired root volume size in GB"
  default     = "64"
}

variable "validator_data_volume_size" {
  description = "Desired chain data volume size in GB"
  default     = "128"
}

variable "monitor_root_volume_size" {
  description = "Desired root volume size in GB"
  default     = "16"
}

variable "monitor_data_volume_size" {
  description = "Desired chain data volume size in GB"
  default     = "16"
}

variable "swap_size" {
  description = "Desired swap file size in GB"
  default     = "2"
}

variable "ubuntu_account_number" {
  description = "AMI owner ID"
  default     = "099720109477"
}

variable "data_snapshot_name" {
  description = "Chain data snapshot name"
}

variable "root_domain" {
  description = "DNS root domain lookup"
}

variable "ip_whitelist" {
  description = "List of ip/cidr to be whitelisted for each ec2 instance"
}

variable "ssh_key_1" {
  description = "ssh key to add to allowed_hosts"
}

variable "ssh_key_2" {
  description = "ssh key to add to allowed_hosts"
}
