# Polkadot Blockchain

## Getting Started

### Example usage
This example will create the following resources in the specified AWS Region:

- Virtual Private Cloud (10.0.0.0/16)
- Internet Gateway
- Route Table
   - Egress all
- Public Subnet (10.0.1.0/24) Zone 1
- Elastic Load Balancer
   - LB https 443
   - Monitor http 3000 (grafana)
- Security Group (ELB)
   - Ingress
      - 443    tcp all
   - Egress
      - all   all all
- Security Group (harmony-node)
   - Ingress
      - 22    tcp all
      - 8081  tcp monitor (polkadot rpc)
      - 26656 udp all     (tendermint p2p)
			- 26657 udp all     (tendermint rpc)
   - Egress
      - all   all all
- Key Pair
- EC2 Instance (Ubuntu Server 18.04 LTS (HVM), SSD Volume Type )

After you run `terraform apply` on this configuration, it will
automatically output the following:

- Monitor IP address
- Monitor instance ID
- Node IP address
- Node instance ID
- Project prefix

For example:

```
terraform apply \
   -var 'key_name=terraform' \
   -var 'swap-size=2' \
   -var 'volume-size=32' \
   -var 'private_key_path=~/.ssh/id_rsa.pub' \
   -var 'public_key_path=~/.ssh/id_rsa.pub'
```

See `variables.tf` for a list of all available variables.